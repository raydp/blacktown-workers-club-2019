<?php 
    $home_dir = get_home_url();
?>

<div class="desktop-menu show-for-large">
	<div style="max-width: 75em; margin: auto; position: relative;">
		<div class="left-area">
			<a href="/">
				<img src="<?php echo $home_dir?>/content/themes/DailyPress/assets/images/logo.png" alt="Blacktown Workers Club Logo" class="site-logo">
			</a>
		</div>
		<div class="right-area">
			<div class="top-section">
				<div class="grid-x" style="justify-content: flex-end; align-items: center;">
					<div class="social-icons">
						<a href="https://www.facebook.com/workersclubgroup/" target="_blank"><img class="svg" src="<?php echo $home_dir?>/content/themes/DailyPress/assets/images/facebook.svg" alt="Facebook link"></a>
						<a href="https://www.instagram.com/workersclubgroup/" target="_blank"><img class="svg" src="<?php echo $home_dir?>/content/themes/DailyPress/assets/images/instagram.svg" alt="Instagram link"></a>
					</div>
					<div class="venue-dropdown">
						<select class="select-style" onChange="window.location.href=this.value" style="background-image: url(/content/themes/DailyPress/assets/images/arrow-down.png);">
							<option>CHANGE VENUE</option>
							<option value="http://blacktown.workersclub.com.au">WORKERS BLACKTOWN</option>
							<option value="http://sports.workersclub.com.au">WORKERS SPORTS</option>
							<option value="http://hubertus.workersclub.com.au">WORKERS HUBERTUS COUNTRY CLUB</option>
						</select>
					</div>
				</div>
			</div>
			<hr>
			<div class="bottom-section">
				<?php dp_plain_menu(); ?>
			</div>
		</div>
	</div>
</div>

<div class="mobile-menu hide-for-large">
	<div class="logo-banner">
		<div class="logo-banner__logo">
			<a href="/">
				<img src="<?php echo $home_dir?>/content/themes/DailyPress/assets/images/logo.png" alt="Blacktown Workers Club Logo" class="site-logo">
			</a>
		</div>
		<!--  hamburger icon -->
		<div class="logo-banner__opener column small-3">
			<a class="hamburger-icon" href="#">
				<span class="line line-1"></span>
				<span class="line line-2"></span>
				<span class="line line-3"></span>
			</a>
		</div>
	</div>
	<div class="menu-banner">
		<div class="menu-banner__menu">
			<?php dp_plain_menu(); ?>
			<div class="social-icons">
				<a href="https://www.facebook.com/workersclubgroup/" target="_blank"><img class="svg" src="<?php echo $home_dir?>/content/themes/DailyPress/assets/images/facebook.svg" alt="Facebook link"></a>
				<a href="https://www.instagram.com/workersclubgroup/" target="_blank"><img class="svg" src="<?php echo $home_dir?>/content/themes/DailyPress/assets/images/instagram.svg" alt="Instagram link"></a>
			</div>
			<div class="venue-dropdown">
				<select class="select-style" onChange="window.location.href=this.value" style="background-image: url(/content/themes/DailyPress/assets/images/arrow-down.png);">
					<option>CHANGE VENUE</option>
					<option value="http://www.google.com.au">WORKERS BLACKTOWN</option>
					<option value="http://www.google.com.au">WORKERS SPORTS</option>
					<option value="http://www.google.com.au">WORKERS HUBERTUS COUNTRY CLUB</option>
				</select>
			</div>
		</div>
	</div>
</div>