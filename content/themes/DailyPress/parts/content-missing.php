<?php
/**
 * The template part for displaying a message that posts cannot be found
 */
?>
<div id="post-not-found" class="hentry not-found">

  <?php if ( is_search() ) : ?>
    <header class="article-header">
      <img class="icon" src="<?php echo get_template_directory_uri(); ?>/assets/images/cancel.svg"/>
      <h2 class="result-title">Sorry, No Results.</h2>
    </header>

    <section class="entry-content">
      <p>Try your search again.</p>
    </section>

    <section class="search">
      <p><?php get_search_form(); ?></p>
    </section> <!-- end search section -->

  <?php else: ?>

  <header class="article-header">
    <h1>Oops, Post Not Found!</h1>
  </header>

  <section class="entry-content">
    <p>Uh Oh. Something is missing. Try double checking things.</p>
  </section>

  <section class="search">
    <p><?php get_search_form(); ?></p>
  </section> <!-- end search section -->
  <?php endif; ?>

</div>
