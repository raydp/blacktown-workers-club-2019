/*
These functions make sure WordPress
and Foundation play nice together.
*/

jQuery(document).ready(function() {

  // Remove empty P tags created by WP inside of Accordion and Orbit
  jQuery('.accordion p:empty, .orbit p:empty').remove();

  // Adds Flex Video to YouTube and Vimeo Embeds
  jQuery('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').each(function() {
    if ( jQuery(this).innerWidth() / jQuery(this).innerHeight() > 1.5 ) {
      jQuery(this).wrap("<div class='widescreen responsive-embed'/>");
    } else {
      jQuery(this).wrap("<div class='responsive-embed'/>");
    }
  });

  /*
   * Replace all SVG images with inline SVG
   */
  jQuery('img.svg').each(function(){
    var $img = jQuery(this);
    var imgID = $img.attr('id');
    var imgClass = $img.attr('class');
    var imgURL = $img.attr('src');

    jQuery.get(imgURL, function(data) {
      // Get the SVG tag, ignore the rest
      var $svg = jQuery(data).find('svg');
      // Add replaced image's ID to the new SVG
      if(typeof imgID !== 'undefined') { $svg = $svg.attr('id', imgID); }
      // Add replaced image's classes to the new SVG
      if(typeof imgClass !== 'undefined') { $svg = $svg.attr('class', imgClass+' replaced-svg'); }
      // Remove any invalid XML tags as per http://validator.w3.org
      $svg = $svg.removeAttr('xmlns:a');
      // Replace image with new SVG
      $img.replaceWith($svg);
    }, 'xml');
  });

  /******
   * Hero Slider
   ******/

  if(jQuery('.slider .slider__container .single-slide').length > 1) {
    jQuery('.slider__container').slick({
      slidesToShow: 1,
      lazyLoad: 'ondemand',
      autoplay: true,
      autoplaySpeed: 5000,
      dots: false,
      pauseOnHover: true,
      prevArrow: '<button class="slick-prev slick-arrow" aria-label="Previous" type="button" style=""><img class="prev-img" src="/content/uploads/2019/11/left-arrow.png"/></button>',
      nextArrow: '<button class="slick-next slick-arrow" aria-label="Next" type="button" style=""><img class="next-img" src="/content/uploads/2019/11/right-arrow.png"/></button>',
      responsive: [
        {
          breakpoint: 480,
          settings: {
              arrows: false,
              draggable: true,
          }
        }
      ]
    });
  }

  /******
   * Body Image Slider
   ******/

  if(jQuery('.body-image-slider .body-image-slider__container .slide').length > 1) {
    jQuery('.body-image-slider__container').slick({
      slidesToShow: 1,
      lazyLoad: 'ondemand',
      autoplay: true,
      autoplaySpeed: 5000,
      dots: false,
      pauseOnHover: true,
      prevArrow: '<button class="slick-prev slick-arrow" aria-label="Previous" type="button" style=""><img class="prev-img" src="/content/uploads/2019/11/left-arrow.png"/></button>',
      nextArrow: '<button class="slick-next slick-arrow" aria-label="Next" type="button" style=""><img class="next-img" src="/content/uploads/2019/11/right-arrow.png"/></button>',
      responsive: [
        {
          breakpoint: 480,
          settings: {
              arrows: false,
              draggable: true,
          }
        }
      ]
    });
  }

  
});

/******
 * Mobile And Desktop Menu: Events and Settings
 ******/

var menuState = ""; //initialize menu state
var navigationBreakpoint = 1024; //nav menu breakpoint is 1440px (xxlarge in Foundation 6)
var mobileDesktopMenuSwitch = debounce(function(){ //active the function every 250ms to improve website performance
  if(menuState !== "mobile" && window.innerWidth < navigationBreakpoint) {
    menuState = "mobile";
    jQuery('.hamburger-icon').on('click', function(e){
      e.preventDefault();
      jQuery(this).toggleClass('active');
      jQuery(this).parents('.mobile-menu').toggleClass('first-level-open');
      jQuery('html').toggleClass('no-scroll');
    });

    /* allow the link(has children) only expand once, then clickable */
    jQuery('.menu-item-has-children').one('click', function(e){
     e.preventDefault();
     jQuery(this).parents('.mobile-menu').toggleClass('second-level-open');
     jQuery(this).toggleClass('open');
   });

  } else if(window.innerWidth >= navigationBreakpoint) {
     menuState = "desktop";
     jQuery('.hamburger-icon').off('click');
     jQuery('.menu-item-has-chilcren > a').off('click');
     jQuery('.second-level-menu .back').off('click');
  }
}, 250);

mobileDesktopMenuSwitch();
window.addEventListener('resize', mobileDesktopMenuSwitch)


/*
 *  jQuery Lazy Load 
 */


jQuery(function() {
  jQuery('.lazy').Lazy({
    effect: 'fadeIn',
    effectTime: 500,
  });
});