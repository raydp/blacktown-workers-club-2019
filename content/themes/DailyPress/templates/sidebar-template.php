<?php
/**
* Template Name: Sidebar Template
*/
?>

<?php get_header(); ?>
    <div id="content" class="<?php echo $post->post_name; ?>">
        <?php
            while(have_rows('page_header')): the_row();
                if(get_row_layout() == 'slider'):
                    get_template_part('templates/snippets/slider');
                endif;
            endwhile;
        ?>
        <div id="main" class="main grid-x">
            <div id="sidebar" class="sidebar small-12 medium-3">
                <?php 
                    while(have_rows('sidebar')): the_row();
                        if(get_row_layout() == 'sidebar_menu'):
                            get_template_part('templates/snippets/sidebar-menu');
                        endif;
                    endwhile;
                ?>
            </div>
            <div id="inner-content" class="inner-content small-12 medium-9">
                <?php
                    while(have_rows('page_body')): the_row();
                        if(get_row_layout() == 'hover_box'):
                            get_template_part('templates/snippets/hover-box');
                        endif;
                        if(get_row_layout() == 'logo_list'):
                            get_template_part('templates/snippets/logo-list');
                        endif;
                        if(get_row_layout() == 'separator'):
                            get_template_part('templates/snippets/separator');
                        endif;
                        if(get_row_layout() == 'two_columns_text_section'):
                            get_template_part('templates/snippets/two-columns-text-section');
                        endif;
                        if(get_row_layout() == 'one_column_text_section'):
                            get_template_part('templates/snippets/one-column-text-section');
                        endif;
                        if(get_row_layout() == 'body_image_slider'):
                            get_template_part('templates/snippets/body-image-slider');
                        endif;
                        if(get_row_layout() == 'notepad'):
                            get_template_part('templates/snippets/notepad');
                        endif;
                        if(get_row_layout() == 'button_group'):
                            get_template_part('templates/snippets/button-group');
                        endif;
                        if(get_row_layout() == 'hover_box_with_content'):
                            get_template_part('templates/snippets/hover-box-with-content');
                        endif;
                        if(get_row_layout() == 'display_weekly_activities'):
                            get_template_part('templates/snippets/display-weekly-activities');
                        endif;
                endwhile;
                ?>
            </div>
        </div>
    </div> <!-- end #content -->

<?php get_footer(); ?>
