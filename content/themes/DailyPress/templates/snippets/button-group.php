<section class="button-group">
    <div class="button-group__container grid-y">
        <?php if( have_rows('button') ): ?>
            <?php while( have_rows('button') ): the_row(); 
                $link = get_sub_field('link');
            ?>
            <a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
                <button class="button"><?php echo $link['title']; ?></button>
            </a>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</section>