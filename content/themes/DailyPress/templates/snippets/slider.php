<section class="slider">
    <div class="slider__container">
        <?php if( have_rows('single_slide') ): ?>
            <?php while( have_rows('single_slide') ): the_row(); 
                $background_image = get_sub_field('image');
                $title = get_sub_field('title');
                $link = get_sub_field('link');
                $image_overlay = '';
                if(get_sub_field('image_overlay')) {
                    $image_overlay = 'image-overlay';
                } else {
                    $image_overlay = 'no-image-overlay';
                }
            ?>
                <div class="single-slide <?php echo $image_overlay; ?>" style="background-image: url(<?php echo $background_image['url']; ?>)">
                    <div class="single-slide__container grid-y">
                        <?php if($title): ?>
                            <h1 class="title"><?php echo $title; ?></h1>
                        <?php endif; ?>
                        <?php if($link): ?>
                            <a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><h4 class="link"><?php echo $link['title']; ?> ></h4></a>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</section>