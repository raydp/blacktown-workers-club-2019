<?php 
    $section_width = 'width: ' . strval(get_sub_field('section_width')) . '%; ';
    $auto_two_columns = '';
    if(get_sub_field('auto_two_columns')) {
        $auto_two_columns = 'auto-two-columns';
    }
?>
<section class="one-column-text-section">
    <div class="one-column-text-section__container row">
        <div class="content <?php echo $auto_two_columns; ?>" style="margin: auto; <?php echo $section_width; ?>">
            <?php echo get_sub_field('content'); ?>
        </div>
    </div>
</section>