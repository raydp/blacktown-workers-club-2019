<section class="name-wall">
    <div class="name-wall__container grid-x">
        <?php if( have_rows('single_box') ): ?>
            <?php while( have_rows('single_box') ): the_row(); 
                $background_image = get_sub_field('background_image');
                $name = get_sub_field('name');
                $title = get_sub_field('title');
            ?>
                <div class="single-box small-12 medium-6 large-4">
                    <div class="background-image lazy" data-src="<?php echo $background_image['url']; ?>"></div>
                    <p class="title"><?php echo $title; ?></p>
                    <h3 class="name"><?php echo $name; ?></h3>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</section>