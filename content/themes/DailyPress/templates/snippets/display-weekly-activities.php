<?php 
    $dir = get_template_directory_uri();
    $title = get_sub_field('title');
    $target_weekday = get_sub_field('weekday', 'option');
    if(!$target_weekday) {
        $target_weekday = "weekly-activities";
    }
?>

<section class="display-weekly-activities">
    <div class="display-weekly-activities__container">
        <div class="title-area">
            <h2 id="weekday-<?php echo $target_weekday; ?>"><?php echo $title; ?></h2>
        </div>
        <div class="activities-list row">
            <?php            
            $posts = new WP_Query(array(
                'post_type'      => 'weekly_activities',
                'category_name'  => $target_weekday,
                'posts_per_page' => -1,
                'meta_query' => array(
                    array(
                        'key' => 'event_date',
                    ), 
                    array(
                        'key' => 'event_time',
                    ),
                ),
                'orderby'        => array(
                    'event_date' => 'ASC',
                    'event_time'   => 'ASC'
                ),
            ));
        
            if($posts->have_posts()) {
                while($posts->have_posts()) {
                    $posts->the_post();
                    if(get_field('tickets') && get_field('tickets_link')){
                        $tickets_status = 'has-tickets';
                    }else{
                        $tickets_status = '';
                    }

                    echo '<div class="single-weekly-activity'. $tickets_status .' small-12 medium-6 large-4">';
                        echo '<div class="single-weekly-activity__container">';
                            echo '<div class="image-area">';
                                echo '<img class="image-background lazy" data-src="' . get_the_post_thumbnail_url(null, 'full') . '">';
                                echo '<div class="image-overlay"></div>';
                            echo '</div>';
                            echo '<div class="content-area">';
                                    echo '<h5>' . get_field('display_title') .'</h5>';
                                    echo '<span class="date-time">' . get_field('event_date') . ' @ ' . get_field('event_time') . '</span>';
                                    echo the_content();                                
                            echo '</div>';
                        echo '</div>';
                    echo '</div>';
                }
            } 
            wp_reset_query();  ?>
        </div>
    </div>
</section>