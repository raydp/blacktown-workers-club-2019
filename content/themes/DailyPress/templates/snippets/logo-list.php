<section class="logo-list">
    <div class="logo-list__container grid-x">
        <?php if( have_rows('single_logo') ): ?>
            <?php while( have_rows('single_logo') ): the_row(); 
                $logo = get_sub_field('logo');
                $link = get_sub_field('link');
            ?>
                <div class="single-logo small-12 medium-3 large-2 text-center">
                    <?php if($link): echo '<a href="' . $link['url'] . '" target="' . $link['target'] . '">'; endif;?>
                        <img class="logo-image" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>">
                    <?php if($link): echo '</a>'; endif;?>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</section>