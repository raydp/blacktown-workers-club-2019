<section class="body-image-slider">
    <div class="body-image-slider__container">
        <?php if( have_rows('slide') ): ?>
            <?php while( have_rows('slide') ): the_row(); 
                $image = get_sub_field('image');
            ?>
            <div class="slide">
                <img class="image" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
            </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</section>