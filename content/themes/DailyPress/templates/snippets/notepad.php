<?php 
    $move_upper = '-' . get_sub_field('move_upper') . 'px';
    $move_lower = '-' . get_sub_field('move_lower') . 'px';
?>
<section class="notepad" style="margin-top: <?php echo $move_upper; ?>; margin-bottom: <?php echo $move_lower; ?>;">
    <div class="notepad__container grid-x">
        <div class="background-color">
            <?php echo get_sub_field('content'); ?>
        </div>
    </div>
</section>