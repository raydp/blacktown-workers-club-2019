<?php 
    $move_upper = '';
    if(get_sub_field('move_upper')) {
        $move_upper = 'move-upper';
    }
?>
<section class="hover-box <?php echo $move_upper; ?>">
    <div class="hover-box__container grid-x">
        <?php if( have_rows('single_box') ): ?>
            <?php while( have_rows('single_box') ): the_row(); 
                $background_image = get_sub_field('background_image');
                $title = get_sub_field('title');
                $link = get_sub_field('link');
            ?>
                <div class="single-box small-12 medium-6 large-4">
                    <div class="background-image lazy" data-src="<?php echo $background_image['url']; ?>">
                        <?php if($link): echo '<a href="' . $link['url'] . '" target="' . $link['target'] . '">'; endif;?>
                        <div class="overlay-chip">
                            <h3 class="title"><?php echo $title; ?></h3>
                        </div>
                        <?php if($link): echo '</a>'; endif;?>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</section>