<?php 
    $left_column = get_sub_field('left_column');
    $right_column = get_sub_field('right_column');
    $left_column_proportion = get_sub_field('left_column_proportion');
    $right_column_proportion = 100 - $left_column_proportion;
?>

<section class="two-columns-text-section">
    <div class="two-columns-text-section__container grid-x">
        <div class="left-column small-12" style="max-width: <?php echo $left_column_proportion; ?>%">
            <?php echo $left_column; ?>
        </div>
        <div class="right-column small-12" style="max-width: <?php echo $right_column_proportion; ?>%">
            <?php echo $right_column; ?>
        </div>
    </div>
</section>