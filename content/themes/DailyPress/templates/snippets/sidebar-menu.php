<div class="sidebar__container">
    <?php if( have_rows('menu_item') ): ?>
        <?php while( have_rows('menu_item') ): the_row(); 
            $highlighted_menu_item = get_sub_field('highlighted_menu_item');
            $link = get_sub_field('link');
        ?>
            <div class="single-menu-item">
                    <a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>" class="<?php if($highlighted_menu_item): echo ' highlight-item'; endif; ?>">
                        <span class="title"><?php echo $link['title']; ?></span>
                    <?php if($link): echo '</a>'; endif;?>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>
</div>