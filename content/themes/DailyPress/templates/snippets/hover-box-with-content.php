<section class="hover-box-with-content">
    <div class="hover-box-with-content__container grid-x">
        <?php if( have_rows('single_item') ): ?>
            <?php while( have_rows('single_item') ): the_row(); 
                $link = get_sub_field('link');
                $image = get_sub_field('image');
                $title = get_sub_field('title');
                $description = get_sub_field('description');
            ?>
            
            <div class="single-item small-12 medium-6">
                <?php if($link): ?><a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php endif; ?>
                    <div class="image-area">
                        <img class="lazy" data-src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
                    </div>
                    <div class="text-area">
                        <h6><?php echo $title; ?></h6>
                        <p><?php echo $description; ?><p>
                    </div>
                <?php if($link): ?></a><?php endif; ?>
            </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</section>