<?php 
    $padding_top = '';
    if(get_sub_field('padding_top')) {
        $padding_top = '20px';
    }
    $padding_bottom = '';
    if(get_sub_field('padding_bottom')) {
        $padding_bottom = '20px';
    }
?>
<section class="separator">
    <div class="separator__container grid-x" style="padding-top: <?php echo $padding_top; ?>; padding-bottom: <?php echo $padding_bottom; ?>;">
        <hr>
    </div>
<section>