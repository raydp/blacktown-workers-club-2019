<?php get_header(); ?>

  <div id="content" class="<?php echo $post->post_name; ?>">
    <?php
      while(have_rows('page_header')): the_row();
        if(get_row_layout() == 'slider'):
          get_template_part('templates/snippets/slider');
        endif;
        // if(get_row_layout() == 'video_slider'):
        //   get_template_part('templates/snippets/video-slider');
        // endif;
      endwhile;
      while(have_rows('page_body')): the_row();
        if(get_row_layout() == 'hover_box'):
          get_template_part('templates/snippets/hover-box');
        endif;
        if(get_row_layout() == 'logo_list'):
          get_template_part('templates/snippets/logo-list');
        endif;
        if(get_row_layout() == 'separator'):
          get_template_part('templates/snippets/separator');
        endif;
        if(get_row_layout() == 'two_columns_text_section'):
          get_template_part('templates/snippets/two-columns-text-section');
        endif;
        if(get_row_layout() == 'one_column_text_section'):
          get_template_part('templates/snippets/one-column-text-section');
        endif;
        if(get_row_layout() == 'body_image_slider'):
          get_template_part('templates/snippets/body-image-slider');
        endif;
        if(get_row_layout() == 'notepad'):
          get_template_part('templates/snippets/notepad');
        endif;
        if(get_row_layout() == 'button_group'):
          get_template_part('templates/snippets/button-group');
        endif;
        if(get_row_layout() == 'hover_box_with_content'):
          get_template_part('templates/snippets/hover-box-with-content');
        endif;
        if(get_row_layout() == 'display_weekly_activities'):
          get_template_part('templates/snippets/display-weekly-activities');
        endif;
        if(get_row_layout() == 'name_wall'):
          get_template_part('templates/snippets/name-wall');
        endif;
      endwhile;
    ?>
  </div> <!-- end #content -->
      <!-- <div class="mover"></div>
      <script>
        let root = document.documentElement;
        
        root.addEventListener("mousemove", e => {
          root.style.setProperty('--mouse-x', e.clientX + 5 + "px");
          root.style.setProperty('--mouse-y', e.clientY + 5 + "px");
        });
      </script> -->
<?php get_footer(); ?>
