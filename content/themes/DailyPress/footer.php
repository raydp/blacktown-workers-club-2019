        <footer class="footer" role="contentinfo">
            <div class="footer__top-section">
                <div class="grid-x top-border">
                    <div class="left-section small-12 medium-2">
                        <div class="footer--title">
                            <h5>Your Club</h5>
                        </div>
                        <div class="footer--content grid-y" style="align-items: flex-start">
                            <?php echo do_shortcode("[dp_custom_menu name='footer-menu']"); ?>
                        </div>
                    </div>
                    <div class="middle-section small-12 medium-6">
                        <div class="footer--title">
                            <h5>Workers Blacktown</h5>
                        </div>
                        <div class="footer--content">
                            <table>
                                <tr><td>55 Campbell Street, Blacktown NSW 2148, Australia</tr></td>
                            </table>
                            <table>
                                <tr><td><strong>SUNDAY TO THURSDAY</strong></td><td>9.00am to 3.00am</td></tr>
                                <tr><td><strong>FRIDAY & SATURDAY</strong></td><td>9.00am to 4.00am</td></tr>
                            </table>
                            <table>
                                <tr><td><a href="tel:02 9830 0600">(02) 9830 0600</a></td></tr>
                                <tr><td><a href="tel:02 9830 0633">(02) 9830 0633</a></td></tr>
                            </table>
                        </div>
                    </div>
                    <div class="right-section small-12 medium-4">
                        <div class="footer--title">
                            <h5>Subscribe To Newsletter</h5>
                        </div>
                        <div class="footer--content">
                            <!--<a href="/">-->
                            <!--    <button class="subscribe-newsletter">SIGN UP!</button>-->
                            <!--</a>-->
                            <div id="sbx_button"></div>
                            <script async="async" defer="defer" src="https://talkbox.impactapp.com.au/signup_buttons/n6_edb39ao5MPCwR9Df9Xg==/script.js" type="text/javascript"></script>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer__bottom-section">
                <div class="grid-x">
                    <div class="left-section small-12 medium-6">
                        <p>WEBSITE BY DAILY PRESS GROUP | COPYRIGHT © <?php echo date('Y') ?>. BLACKTOWN WORKERS CLUB GROUP</p>
                    </div>
                    <div class="right-section small-12 medium-6">
                        <p class="text-right">THINK! ABOUT YOUR CHOICES CALL GAMBLING HELP <a href="tel: 1800 858 858">1800 858 858</a> <a href="www.gamblinghelp.nsw.gov.au" target="_blank">WWW.GAMBLINGHELP.NSW.GOV.AU</a></p>
                    </div>
                </div>
            </div>
            
        </footer> <!-- end .footer -->

        <?php wp_footer(); ?>

    </body>

</html> <!-- end page -->
