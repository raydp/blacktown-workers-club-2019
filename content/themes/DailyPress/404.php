<?php get_header(); ?>
  <div id="content">
     <article id="content-not-found" style="text-align: center; background: #f7f5f5; padding: 50px 20px;">

       <img class="image" src="<?php echo get_template_directory_uri(); ?>/assets/images/404_image.svg" alt="page-not-found-image" style="max-width: 700px;padding-bottom: 50px;"/>
       <h2>Error 404 - Page not found</h2>
       <h4>The page you requested could not be found</h4>

     </article> <!-- end article -->
  </div> <!-- end #content -->
<?php get_footer(); ?>
