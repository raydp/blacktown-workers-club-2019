<?php get_header(); ?>

<div id="blog-post" class="<?php echo $post->post_name; ?>">

    <div class="body row">
      <div class="content">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
          <?php get_template_part( 'templates/custom-layout/single-blog' ); ?>
        <?php endwhile; else : ?>
          <?php get_template_part( 'parts/content', 'missing' ); ?>
        <?php endif; ?>
      </div>
    </div>
</div>

<?php get_footer(); ?>
