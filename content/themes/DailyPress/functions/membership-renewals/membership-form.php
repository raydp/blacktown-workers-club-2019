<?php
/***************************
 * MEMBERSHIP FUNCTIONS
 **************************/
function createMembershipForm() {
  return formHTML();
}
add_shortcode('dp-membership-form', 'createMembershipForm');

function formHTML() { ob_start(); ?>
  <div class="dp-membership-form">
    <form action="" method="post">
      <div class="row">

        <div class="small-12 medium-12 columns">
          <h3>Personal Information</h3>
        </div>

        <div class="small-12 medium-6 columns">
          <label> First Name *
            <input name="first-name" class="required" type="text" />
            <span class="warning"></span>
          </label>
        </div>

        <div class="small-12 medium-6 columns">
          <label> Last name *
            <input name="last-name" class="required" type="text" />
            <span class="warning"></span>
          </label>
        </div>

        <div class="small-12 medium-6 columns">
          <label> Date of Birth *
            <input name="date-of-birth" placeholder="dd/mm/yyyy" class="required" type="text" />
            <span class="warning"></span>
          </label>
        </div>

        <div class="small-12 medium-6 columns">
          <label> Occupation *
            <input name="occupation" type="text" class="required"/>
            <span class="warning"></span>
          </label>
        </div>

        <div class="small-12 medium-6 columns">
          <label> Email Address *
            <input name="email-address" class="required" type="email" />
            <span class="warning"></span>
          </label>
        </div>

        <div class="small-12 medium-6 columns">
          <label> Phone Number
            <input name="phone-number" type="text" />
            <span class="warning"></span>
          </label>
        </div>

        <div class="small-12 medium-12 columns clearfix">
          <h3>Home Address</h3>
        </div>

        <div class="small-12 medium-12 columns">
          <label> Street Address
            <input name="street-address" type="text" />
            <span class="warning"></span>
          </label>
        </div>

        <div class="small-12 medium-6 columns">
          <label> Suburb
            <input name="suburb" type="text" />
            <span class="warning"></span>
          </label>
        </div>

        <div class="small-12 medium-6 columns">
          <label> Postcode
            <input name="postcode" type="text" />
            <span class="warning"></span>
          </label>
        </div>

        <div class="small-12 column medium-12">
          <h3>Membership Options</h3>
        </div>

        <div class="small-12 medium-6 columns">
          <label> Membership Number *
            <input name="membership-number" class="required" type="text" />
            <span class="warning"></span>
          </label>
        </div>

        <div class="small-12 medium-6 columns">
         <label> Membership Category
           <select name="membership-category" class="membership-category">
             <option value="6">1 Year $6.00</option>
             <!-- option value="5.5">1 Year Pensioner $5.50</option -->
             <option value="20">5 Years $20.00</option>
             <!-- option value="22.5">5 Years Pensioner $22.50</option -->
             <option value="50">25 Years $50.00</option>
           </select>
           <span class="warning"></span>
          </label>
        </div>

        <div class="small-12 medium-6 columns">
          <label> Mortality Fund
            <select name="mortality-fund" class="mortality-fund">
              <option value="0">Not Applicable</option>
              <option value="5.5">1 Year $5.50</option>
              <option value="27.5">5 Years $27.50</option>
              <option value="137.5">25 Years $137.50</option>
            </select>
            <span class="warning"></span>
          </label>
        </div>

        <div class="small-12 medium-6 columns">
          <label> Membership Card Delivery
            <select name="membership-card-delivery">
              <option value="by_post">By Post</option>
              <option value="pick_up">Pick Up</option>
            </select>
            <span class="warning"></span>
          </label>
        </div>

        <div class="small-12 medium-12 columns">
          <h3>Getting to know you</h3>
        </div>

        <div class="small-12 medium-12 columns">
          <p class="checkbox-title">Are you a member of a sporting body? <small>(Optional)</small></p>
          <div class="checkboxes">
            <label><input type="checkbox" name="sporting-body[]" value="baseball">Baseball</label>
            <label><input type="checkbox" name="sporting-body[]" value="golf">Golf</label>
            <label><input type="checkbox" name="sporting-body[]" value="bowls">Bowls</label>
            <label><input type="checkbox" name="sporting-body[]" value="rugby-league">Rugby League</label>
            <label><input type="checkbox" name="sporting-body[]" value="cricket">Cricket</label>
            <label><input type="checkbox" name="sporting-body[]" value="soccer-football">Soccer/Football</label>
          </div>
        </div>

        <div class="small-12 medium-12 columns">
          <p class="checkbox-title">Preferred Cuisine <small>(Optional)</small></p>
          <div class="checkboxes">
            <label><input type="checkbox" name="preferred-cuisine[]" value="american">American</label>
            <label><input type="checkbox" name="preferred-cuisine[]" value="asian">Asian</label>
            <label><input type="checkbox" name="preferred-cuisine[]" value="greek">Greek</label>
            <label><input type="checkbox" name="preferred-cuisine[]" value="indian">Indian</label>
            <label><input type="checkbox" name="preferred-cuisine[]" value="italian">Italian</label>
            <label><input type="checkbox" name="preferred-cuisine[]" value="mexican">Mexican</label>
          </div>
        </div>

        <div class="small-12 medium-12 columns">
          <p class="checkbox-title">Preferred Entertainment <small>(Optional)</small></p>
          <div class="checkboxes">
            <label><input type="checkbox" name="preferred-entertainment[]" value="rock-n-roll">Rocn n Roll</label>
            <label><input type="checkbox" name="preferred-entertainment[]" value="disco">Disco</label>
            <label><input type="checkbox" name="preferred-entertainment[]" value="jazz">Jazz</label>
            <label><input type="checkbox" name="preferred-entertainment[]" value="dance-house">Dance/House</label>
            <label><input type="checkbox" name="preferred-entertainment[]" value="classical-opera">Classical/Opera</label>
            <label><input type="checkbox" name="preferred-entertainment[]" value="country-western">Country/Western</label>
          </div>
        </div>

        <div class="small-12 medium-12 columns">
          <p class="checkbox-title">Preferred Sporting Activities <small>(Optional)</small></p>
          <div class="checkboxes">
            <label><input type="checkbox" name="preferred-sporting-activities[]" value="a-league">A-League</label>
            <label><input type="checkbox" name="preferred-sporting-activities[]" value="afl">AFL</label>
            <label><input type="checkbox" name="preferred-sporting-activities[]" value="nrl">NRL</label>
            <label><input type="checkbox" name="preferred-sporting-activities[]" value="boxing-ufc">Boxing/UFC</label>
            <label><input type="checkbox" name="preferred-sporting-activities[]" value="cricket">Cricket</label>
            <label><input type="checkbox" name="preferred-sporting-activities[]" value="horse-racing">Horse Racing</label>
          </div>
        </div>

        <div class="small-12 medium-12 columns" style="margin-bottom: 2rem;">
          <p class="checkbox-title">Venue Interests <small>(Optional)</small></p>
          <div class="checkboxes">
            <label><input type="checkbox" name="venue-interests[]" value="keno">Keno</label>
            <label><input type="checkbox" name="venue-interests[]" value="bingo">Bingo</label>
            <label><input type="checkbox" name="venue-interests[]" value="cash-housie">Cash Housie</label>
            <label><input type="checkbox" name="venue-interests[]" value="social-gaming">Social Gaming</label>
            <label><input type="checkbox" name="venue-interests[]" value="tab">TAB</label>
            <label><input type="checkbox" name="venue-interests[]" value="promotions">Promotions</label>
            <label><input type="checkbox" name="venue-interests[]" value="watch-sports">Watch Sports</label>
            <label><input type="checkbox" name="venue-interests[]" value="raffles">Raffles</label>
            <label><input type="checkbox" name="venue-interests[]" value="functions">Functions</label>
            <label><input type="checkbox" name="venue-interests[]" value="dining">Dining</label>
            <label><input type="checkbox" name="venue-interests[]" value="meet-friends">Meet Friends</label>
            <label><input type="checkbox" name="venue-interests[]" value="gaming-machines">Gaming Machines</label>
            <label><input type="checkbox" name="venue-interests[]" value="live-entertainment">Live Entertainment</label>
            <label><input type="checkbox" name="venue-interests[]" value="kids-entertainment">Kids Entertainment</label>
            <label><input type="checkbox" name="venue-interests[]" value="health-club-gym">Health Club/Gym</label>
          </div>
        </div>

        <div class="small-12 medium-12 columns">
          <h3>Do you wish to receive a copy of the following?</h3>
        </div>

        <div class="small-12 medium-12 columns">
          <label> Annual General Meeting Notice
            <select name="annual-general-meeting-notice">
              <option value="no" selected="selected">No</option>
              <option value="email">Email</option>
              <option value="mail">Mail</option>
            </select>
            <span class="warning"></span>
          </label>
        </div>

        <div class="small-12 medium-12 columns">
          <label> Membership Renewal Notice
            <select name="membership-renewal-notice">
              <option value="email" selected="selected">Email</option>
              <option value="mail">Mail</option>
            </select>
            <span class="warning"></span>
          </label>
        </div>

        <div class="small-12 medium-12 columns">
          <label> Annual Report
            <select name="annual-report">
              <option value="no" selected="selected">No (Available Online)</option>
              <option value="mail">Mail</option>
            </select>
            <span class="warning"></span>
          </label>
        </div>

        <div class="small-12 medium-12 columns">
          <label> Newsletter (includes Journal)
            <select name="newsletter">
              <option value="no" selected="selected">No</option>
              <option value="email">Email</option>
              <option value="mail">Mail</option>
            </select>
            <span class="warning"></span>
          </label>
        </div>

        <div class="small-12 medium-12 columns">
          <label> Promotions
            <select name="promotions">
              <option value="no" selected="selected">No</option>
              <option value="email">Email</option>
              <option value="mail">Mail</option>
            </select>
            <span class="warning"></span>
          </label>
        </div>

        <div class="small-12 medium-12 columns">
          <label>
            <input type="checkbox" name="promotions-declaration">I wish to receive promotional material including offers relating to promotions and special events which may include gaming material.
          </label>
        </div>
        <div class="small-12 medium-12 columns submit-container" style="padding-top: 20px;">
          <input type="hidden" name="action" value="saveMemberData"/>
          <button type="submit" class="button submit-button">Submit</button>
        </div>
      </div><!-- End of Form -->
    </form>
  </div>
  <script>
    jQuery('.dp-membership-form form').submit(function(e){
      e.preventDefault();
      if(validateForm()) {
        saveData();
        jQuery('.dp-membership-form .submit-button').attr('disabled', 'disabled');
        jQuery('.dp-membership-form .submit-container').append('<img class="loading-icon" src="<?php echo get_template_directory_uri(); ?>/functions/membership-renewals/loading-icon.svg"/>');
      }
    });

    function saveData() {
      var memberData = jQuery('.dp-membership-form form').serialize();

      jQuery.ajax({
        type : "POST",
        url  : "/wp/wp-admin/admin-ajax.php",
        data : memberData,
        success : function (response) {
          var url = '<?php echo get_home_url();?>/membership/membership-checkout';
          var form = jQuery(
            '<form action="' + url + '" method="post" style="display: none;">' +
              '<input type="hidden" name="post-id" value="' + response.data.post_id + '" />' +
            '</form>');
          jQuery('body').append(form);
          form.submit();
        }
      });
    }

    function validateForm() {
      jQuery('.warning').html('');
      return validateRequired();
    }

    function validateRequired() {
      var allValid = true;
      jQuery('.required').each(function(){
        if(jQuery(this).val().length === 0) {
          jQuery(this).siblings('.warning').html("This field must not be empty");
          jQuery(this).focus();
          allValid = false;
        }
      });
      return allValid;
    }
  </script>
<?php
  return ob_get_clean();
}

function saveMemberData() {
  $first_name = $_POST['first-name'];
  $last_name = $_POST['last-name'];
  if(!empty($_POST['post-id'])) {
    $post_id = $_POST['post-id'];
  }
  $post_id = wp_insert_post(array(
    "post_title" => $first_name.' '.$last_name,
    "post_type" => 'membership_post_type',
    "post_status" => 'publish'
  ));

  foreach($_POST as $key => $value) {
    add_post_meta($post_id, str_replace('-','_', $key), $value, true);
  }
  $date_of_birth = date('Ymd', strtotime($_POST['date-of-birth']));
  add_post_meta($post_id, 'date_of_birth', $date_of_birth, true);

  $return = array(
    'post_id' => $post_id
  );
  wp_send_json_success($return);
}
add_action('wp_ajax_saveMemberData', 'saveMemberData');
add_action('wp_ajax_nopriv_saveMemberData', 'saveMemberData');



/*************************
 * CHECKOUT FUNCTIONS
 *************************/
function createCheckout() {
  return checkoutHTML();
}
add_shortcode('dp-membership-checkout', 'createCheckout');

function checkoutHTML() { ob_start(); ?>
  <div class="checkout-container">
    <?php if(isset($_POST['post-id'])) :?>
      <div class="details-confirmation">
        <?php $member_post_id = $_POST['post-id']; ?>
        <div class="">
          <h3>Checkout</h3>
        </div>
        <table>
          <tbody>
            <tr>
              <td>First Name</td>
              <td><?php echo get_field('first_name', $member_post_id); ?></td>
            </tr>
            <tr>
              <td>Last Name</td>
              <td><?php echo get_field('last_name', $member_post_id); ?></td>
            </tr>
            <tr>
              <td>Membership Number</td>
              <td><?php echo get_field('membership_number', $member_post_id); ?></td>
            </tr>
            <tr>
              <td>Membership Category</td>
              <td class="membership-category-container"><?php echo get_field('membership_category', $member_post_id); ?></td>
            </tr>
            <tr>
              <td>Mortality Fund</td>
              <td class="mortality-fund-container"><?php echo get_field('mortality_fund', $member_post_id); ?></td>
            </tr>
            <tr>
              <td><strong>Total</strong></td>
              <?php $total_amount = floatval(get_field('membership_category', $member_post_id)) + floatval(get_field('mortality_fund', $member_post_id)); ?>
              <td><strong>$<?php echo $total_amount; ?></strong></td>
            </tr>
          </tbody>
        </table>

        <div class="make-payment">
          <form action="<?php echo get_home_url();?>/membership/membership-thankyou" class="payment-form" method="post">
            <input type="hidden" name="action" value="processPayment"/>
            <input type="hidden" class="post-id" name="post-id" value="<?php echo $member_post_id;?>"/>
            <input type="hidden" name="full-name" value="<?php echo get_field('first_name', $member_post_id).' '.get_field('last_name', $member_post_id);?>"/>
            <input type="hidden" name="amount" value="<?php echo $total_amount; ?>"/>

            <div class="row">
              <div class="payment-title">
                <h3>Make Payment</h3>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/visa-mastercard.png" alt="Visa & Mastercard Logo" style="max-width: 230px;"/>
              </div>
              <span class="payment-error" style="color: red;"></span>
              <div class="medium-12 columns">
                <label> Card Number *
                  <input name="ACCT" class="required" type="text" maxlength="19" value="" />
                  <span class="warning"></span>
                </label>
              </div>
              <div class="medium-6 columns">
                <label> Card Expiry (mmyy) *
                  <input name="EXPDATE" class="required" type="text" maxlength="4" value="" />
                  <span class="warning"></span>
                </label>
              </div>
              <div class="medium-6 columns">
                <label> CVV *
                  <input name="CVV" class="required" type="text" maxlength="3" value="" />
                  <span class="warning"></span>
                </label>
              </div>
              <div class="medium-12 columns submit-container">
                <button type="submit" class="button submit-button">Submit</button>
              </div>
            </div>
          </form>
        </div>

      </div>
    <?php else: ?>
      <div class="wrong-page">
        Oops, It seems you come to a wrong page. Please go to <a href="<?php echo get_home_url();?>/membership-application">membership </a> page for more information about membership.
      </div>
    <?php endif; ?>
  </div>

  <script>
    //Change table pricing details
    //REMEMBER!! There is another PHP array like this below
    var membershipCategory = {
      '6': '1 Year $6.00',
      //'5.5': '1 Year Pensioner $5.50',
      '20': '5 Year $20.00',
      //'22.5': '5 Year Pensioner $22.50'
      '50': '25 Year $50.00',
    };

    var mortalityFund= {
      '0': 'Not Applicable',
      '5.5': '1 Year $5.50',
      '27.5': '5 Year $27.50',
      '137.5': '25 Year $137.50'
    };

    var memCatElement = jQuery('.membership-category-container');
    memCatElement.text(membershipCategory[memCatElement.text()]);

    var mortFundElement = jQuery('.mortality-fund-container');
    mortFundElement.text(mortalityFund[mortFundElement.text()]);

    //process payment method
    jQuery('form.payment-form').submit(function(e){
      e.preventDefault();
      jQuery('form.payment-form .payment-error').html('');

      if(validateForm()) {
        processPayment();
        jQuery('form.payment-form .submit-button').attr('disabled', 'disabled');
        jQuery('form.payment-form .submit-container').append('<img class="loading-icon" src="<?php echo get_template_directory_uri(); ?>/functions/membership-renewals/loading-icon.svg"/>');
      }
    });

    function processPayment() {
      var paymentDetails = jQuery('form.payment-form').serialize();

      jQuery.ajax({
        type : "POST",
        url  : "/wp/wp-admin/admin-ajax.php",
        data : paymentDetails
      }).always(function (response) {
	console.log(response);
        if(response.success === false) {
          jQuery('form.payment-form .payment-error').html(response.data.message);

          //remove loader
          jQuery('form.payment-form .submit-button').removeAttr('disabled');
          jQuery('form.payment-form .submit-container .loading-icon').remove();

        } else {
          //var responseJSON= JSON.parse(response.responseText.substring(5));
          if(response.success === false) {
            jQuery('form.payment-form .payment-error').html(response.data.message);

            //remove loader
            jQuery('form.payment-form .submit-button').removeAttr('disabled');
            jQuery('form.payment-form .submit-container .loading-icon').remove();
          } else {
            //go to thankyou page
            var url = '<?php echo get_home_url();?>/membership/membership-thankyou';
            var form = jQuery(
              '<form action="' + url + '" method="post" style="display: none;">' +
                '<input type="hidden" name="post-id" value="' + response.data.post_id+ '" />' +
              '</form>');
            jQuery('body').append(form);
            form.submit();
          }
        }
      });
    }

    function validateForm() {
      jQuery('.warning').html('');
      return validateRequired();
    }

    function validateRequired() {
      var allValid = true;
      jQuery('.required').each(function(){
        if(jQuery(this).val().length === 0) {
          jQuery(this).siblings('.warning').html("This field must not be empty");
          jQuery(this).focus();
          allValid = false;
        }
      });
      return allValid;
    }
  </script>

<?php
  return ob_get_clean();
}

function processPayment() {
  //check if all field is filled
  if ( empty($_POST['post-id']) && empty($_POST['full-name']) && empty($_POST['amount']) && empty($_POST['ACCT']) && empty($_POST['EXPDATE']) && empty($_POST['CVV'])) {
    $return = array(
      'message' =>  'some field is empty'
    );
    wp_send_json_error($return);
    return;
  }

  require('payflow_curl.php'); //import payflow library
  $post_id = $_POST['post-id'];
  $full_name = $_POST['full-name'];
  $amount = $_POST['amount'];
  $acct = $_POST['ACCT'];
  $expdate = $_POST['EXPDATE'];
  $cvv = $_POST['CVV'];

  $payflow = new payflow("davidhiggins", "davidhiggins", "VSA", "workers2148");

  if ($payflow->get_errors()) {
    $return = array(
      'message' => $payflow->get_errors()
    );
    wp_send_json_error($return);
    return;
  }

  /**
  * Sale Transactions
  * params: card number, card expire date, amount, currency (default USD), extra parameters
  * return: success: response array, fail: false - error message
  */

  // extra params
  $data_array = array(
    'comment1' => 'Membership Renewal',
    'comment2' => $full_name,
    'cvv' => $cvv // for cvv validation response
  );
  $result = $payflow->sale_transaction($acct, $expdate, $amount, "AUD", $data_array);
  if (!$payflow->get_errors()) {

    //update payment id in the database
    if(!add_post_meta($post_id, 'payment_id', $result['PNREF'], true)) {
      update_post_meta($post_id, 'payment_id', $result['PNREF']);
    }

    $return = array(
      'post_id' => $post_id,
      'amount' => $amount,
      'result' => $result
    );
    wp_send_json_success($return);

    return;
  } else {

    $return = array(
      'message' => 'Error processing payment: '.$payflow->get_errors().'<br />Please try again. If problems persist please contact your card issuer.'
    );
    wp_send_json_error($return);
    return;
  }

}
add_action('wp_ajax_processPayment', 'processPayment');
add_action('wp_ajax_nopriv_processPayment', 'processPayment');


/*******************************
 * THANK YOU FUNCTIONS
 ******************************/
function createThankYou() {
  return thankYouHTML();
}
add_shortcode('dp-membership-thankyou', 'createThankYou');

function thankYouHTML() { ob_start(); ?>
  <?php if(isset($_POST['post-id'])): ?>
    <div class="membership-thankyou">
      <small>NOTE: Please do not refresh the page in any of the renewals process step</small>
      <div class="thank-you">
        <h2>Thank You, <?php echo get_field('first_name', $_POST['post-id']); ?></h2>
        <p>
          Thank you for renewing your membership with us. Your Payment ID is <strong><?php echo get_field('payment_id', $_POST['post-id']); ?></strong>.<br>
          <?php echo sendMail($_POST['post-id']); ?>
        </p>
      </div>
    </div>
  <?php else: ?>
    <div class="membership-thankyou">
      <div class="wrong-page">
        Oops, It seems you come to a wrong page. Please go to <a href="<?php echo get_home_url();?>/membership-application">membership </a> page for more information about membership.
      </div>
    </div>
  <?php endif; ?>
<?php
  return ob_get_clean();
}

function sendMail($post_id) {
  //REMEMBER!! There is another Javascript array like this above
  $membership_category = array(
    '6' => '1 Year $6.00',
    //'5.5' => '1 Year Pensioner $5.50',
    '20' => '5 Years $20.00',
    //'22.5' => '5 Year Pensioner $22.50',
    '50' => '25 Years $50.00'
  );

  $mortality_fund= array(
    '0' => 'Not Applicable',
    '5.5' => '1 Year $5.50',
    '27.5' => '5 Years $27.50',
    '137.5' => '25 Years $137.50'
  );

  $sporting_body = !empty(get_field('sporting_body', $post_id)) ? implode(', ', get_field('sporting_body', $post_id)) : '';
  $preferred_cuisine = !empty(get_field('preferred_cuisine', $post_id)) ? implode(', ', get_field('preferred_cuisine', $post_id)) : '';
  $preferred_entertainment = !empty(get_field('preferred_entertainment', $post_id)) ? implode(', ', get_field('preferred_entertainment', $post_id)) : '';
  $preferred_sporting_activities = !empty(get_field('preferred_sporting_activities', $post_id)) ? implode(', ', get_field('preferred_sporting_activities', $post_id)) : '';
  $venue_interests = !empty(get_field('venue_interests', $post_id)) ? implode(', ', get_field('venue_interests', $post_id)) : '';


  $to = get_option('membership_notification_email');
  $subject = 'Membership Renewals';
  $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Workers System <no-reply@workersclub.com.au>');

  $body  = 'You have got a new membership renewals request for: <br><br>';
  $body .= '<strong>Member Information </strong><br>';
  $body .= 'Name: '.get_field('first_name', $post_id).' '.get_field('last_name',$post_id).'<br>';
  $body .= 'DOB: '.date('d-m-Y', strtotime(get_field('date_of_birth', $post_id))).'<br>';
  $body .= 'Occupation: '.get_field('occupation', $post_id).' <br>';
  $body .= 'Phone Number: '.get_field('phone_number', $post_id).' <br>';
  $body .= 'Email Address: '.get_field('email_address', $post_id).' <br>';
  $body .= 'Address: '.get_field('street_address', $post_id).', '.get_field('suburb', $post_id).', '.get_field('postcode', $post_id).' <br><br>';

  $body .= '<strong>Membership Details</strong><br>';
  $body .= 'Membership Number: '.get_field('membership_number', $post_id).'<br>';
  $body .= 'Renewal Membership Category: '.$membership_category[get_field('membership_category', $post_id)].'<br>';
  $body .= 'Mortality Fund: '.$mortality_fund[get_field('mortality_fund', $post_id)].'<br>';
  $body .= 'Delivery of Membership Card: '.get_field('membership_card_delivery', $post_id).'<br><br>';

  $body .= '<strong>Member Interest</strong><br>';
  $body .= 'Sporting Body Membership: '. $sporting_body .'<br>';
  $body .= 'Preferred Cuisine: '. $preferred_cuisine .'<br>';
  $body .= 'Preferred Entertainment: '. $preferred_entertainment .'<br>';
  $body .= 'Preferred Sporting Activities: '. $preferred_sporting_activities .'<br>';
  $body .= 'Venue Interests: '. $venue_interests .'<br><br>';

  $body .= '<strong>Subscription to Publications</strong><br>';
  $body .= 'Annual General Meeting Notice: '.get_field('annual_general_meeting_notice', $post_id).'<br>';
  $body .= 'Membership Renewal: '.get_field('membership_renewal_notice', $post_id).'<br>';
  $body .= 'Annual Report: '.get_field('annual_report', $post_id).'<br>';
  $body .= 'Newsletters: '.get_field('newsletter', $post_id).'<br>';
  $body .= 'Promotions: '.get_field('promotions', $post_id).'<br>';
  $body .= 'Promotions Declaration: '. (get_field('promotions_declaration', $post_id) ? 'yes' : 'no') .'<br>';
  $body .= 'Payment ID: '.get_field('payment_id', $post_id).'<br>';

  if(wp_mail($to, $subject, $body, $headers)) {
    return 'Your membership renewals is currently being procceses.';
  } else {
    return 'There is something wrong in our system. Please email us on '.get_option('membership_notification_email').' with your Name, Member ID, and Payment ID for us to track the problem.';
  }
}
