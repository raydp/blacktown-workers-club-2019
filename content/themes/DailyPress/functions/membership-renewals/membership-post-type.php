<?php
//create the function for the custom type
function membership_post_type() {
	// creating (registering) the custom type
	register_post_type( 'membership_post_type', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	//add all the options for this post type
		array('labels' => array(
			'name' => __('Membership List', 'dailypress'), /* This is the Title of the Group */
			'singular_name' => __('Member', 'dailypress'), /* This is the individual type */
			'all_items' => __('Members', 'dailypress'), /* the all items menu item */
			'add_new' => __('Add New', 'dailypress'), /* The add new menu item */
			'add_new_item' => __('Add New Member', 'dailypress'), /* Add New Display Title */
			'edit' => __( 'Edit', 'dailypress' ), /* Edit Dialog */
			'edit_item' => __('Edit Member', 'dailypress'), /* Edit Display Title */
			'new_item' => __('New Member', 'dailypress'), /* New Display Title */
			'view_item' => __('View Member Detail', 'dailypress'), /* View Display Title */
			'search_items' => __('Search Member', 'dailypress'), /* Search Custom Type Title */
			'not_found' =>  __('Nothing found in the Database.', 'dailypress'), /* This displays if there are no entries yet */
			'not_found_in_trash' => __('Nothing found in Trash', 'dailypress'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Database of membership registered from the website', 'dailypress' ), /* Custom Type Description */
			'public' => false,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' => 'dashicons-id', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'membereship', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => false, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title' )
	 	) /* end of options */
	); /* end of register post type */
}

// adding the function to the Wordpress init
add_action( 'init', 'membership_post_type');

/**
 * Adds a submenu page membership list
 */
function membership_settings() {
    add_submenu_page(
        'edit.php?post_type=membership_post_type',
        __( 'Membership Renewals Setting', 'dailypress' ),
        __( 'Settings', 'dailypress' ),
        'manage_options',
        'membership_settings',
        'membership_settings_callback'
    );
}
add_action('admin_menu', 'membership_settings');

/**
 * Display callback for the submenu page.
 */
function membership_settings_callback() {
  ?>
  <div class="wrap">
    <h1>Membership Renewals Setting</h1>
    <div id="setting-error-settings_updated" class="updated settings-error notice" style="display: none;"> </div>
    <form class="membership-settings" method="post">
      <input type="hidden" name="action" value="save_membership_settings" />
      <table class="form-table">
       <tr>
         <th scope="row"><label for="membership_notification_email">Notifications Email: </label></th>
         <td><input type="text" name="membership_notification_email" size="45" value="<?php echo get_option('membership_notification_email'); ?>" /></td>
       </tr>
      </table>
      <p class="submit">
        <input type="submit" name="Submit" class="button button-primary" value="Save" />
      </p>
    </form>
  </div>
  <script>
    jQuery('.membership-settings').submit(function(e){
      e.preventDefault();
      saveData();
      jQuery('.submit .button').attr('disabled','disabled');
      jQuery('.submit').append('<img class="loading-icon" src="<?php echo get_template_directory_uri(); ?>/functions/membership-renewals/loading-icon.svg" style="width: 30px; vertical-align: top;"/>');
      jQuery('.updated.settings-error').html('').hide();
    });

    function saveData() {
      var membershipSettings = jQuery('.membership-settings').serialize();

      jQuery.ajax({
        type : "POST",
        url  : "/wp/wp-admin/admin-ajax.php",
        data : membershipSettings,
        success : function (response) {
          jQuery('.submit .button').removeAttr('disabled');
          jQuery('.submit .loading-icon').remove();
          jQuery('.updated.settings-error').html('<p><strong>Settings saved.</strong></p>').show();
        }
      });
    }
  </script>
  <?php
}

function save_membership_settings() {
  update_option('membership_notification_email',$_POST['membership_notification_email'],'', 'yes');
  wp_send_json_success();
}
add_action('wp_ajax_save_membership_settings', 'save_membership_settings');

