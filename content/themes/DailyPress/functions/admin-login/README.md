# DP Admin Login

DP Admin Login is a customized admin login page for every WordPress Custom Theme project of Daily Press.

Once you include the DP_AdminLogin.php in your functions.php it will suddenly override the default admin login page of WordPress.

## Installation

Download and extract this repository in your WordPress theme.

```path
project-name > wp-content > theme > your-theme > dp-admin-logo
```

or if you use the clean-wp-theme of Daily Press paste it in

```path
project-name > content > theme > DailyPress > dp-admin-logo
```

## Usage

Include DP_AdminLogin.php in your functions.php

```php
require_once(get_template_directory().'/dp-admin-login/DP_AdminLogin.php');
```

But if you place the whole module under your subfolder, it should be like this

```php
require_once(get_template_directory().'/your-subfolder/dp-admin-login/DP_AdminLogin.php');
```

then update the `MODULE_PATH` in `dp-admin-login/DP_AdminLogin.php` if you really place it in subfolder, check the below code for your reference

```php
const MODULE_PATH = '/your-subfolder/dp-admin-login';
```