( function( $ ) {
  /* Get the login form html elements */
  var login_form = $( '.wp-core-ui #login' ).html();

  /* Empty the login form */
  $( '.wp-core-ui #login' ).empty();

  /* Reconstruct the login form */
  var el = '';
  el += '<div class="__dp_login_form">';
    el += '<div class="__dp_login_header">';
      el += '<a href="https://dailypress.com.au/" target="_blank"><div class="__dp_logo"></div></a>';
    el += '</div>';
    el += '<div class="__dp_login_form_inner">';
      el += '<div>';
        el += '<div>';
          el += login_form;
        el += '</div>';
      el += '</div>';
    el += '</div>';
  el += '</div>';
  el += '<div class="__dp_login_image"></div>';
  $( '.wp-core-ui #login' ).html( el );

  /* Remove the default icon */
  $( '.wp-core-ui #login .__dp_login_form h1' ).remove();

  /* Add title */
  $( '.wp-core-ui #login .__dp_login_form #loginform' ).before( '<h2>Login your account</h2>' );
  $( '.wp-core-ui #login .__dp_login_form #lostpasswordform' ).before( '<h2>Forgot your password?</h2>' );

  /* Change label */
  $( '.wp-core-ui #login .__dp_login_form #lostpasswordform p:first-child label' ).text( 'Enter your registered E-Mail Address' );

  /* Add placeholder */
  $( '.wp-core-ui #login .__dp_login_form #user_login' ).attr( 'placeholder', 'Your E-Mail' );
  $( '.wp-core-ui #login .__dp_login_form #user_pass' ).attr( 'placeholder', 'Your Password' );

  /** Animation Functions **/
  function initializeAnimation(className, json) {
    var container = document.getElementsByClassName(className)[0];
    container.innerHTML = ""; //empty the content

    var animation = lottie.loadAnimation({
      container: container,
      renderer: 'svg',
      loop: true,
      autoplay: false,
      path: localized_obj.dir + '/assets/js/' + json
    });

    return animation;
  }

  function registerEvent(animation, loadedEvent, mouseEnterEvent, clickEvent, mouseLeaveEvent) {
    if(loadedEvent == undefined) {
      loadedEvent = function() {
        animation.playSegments([0, 74], true);
        animation.loop=true;
      }
    }

    if(mouseEnterEvent == undefined) {
      mouseEnterEvent = function() {
        animation.playSegments([75, 124], true);
      }
    }

    if(clickEvent == undefined) {
      clickEvent = function() {
        animation.playSegments([125, 149], true);
        animation.loop=false;
      }
    }

    if(mouseLeaveEvent == undefined) {
      mouseLeaveEvent = function() {
        animation.playSegments([0, 74], true);
        animation.loop=true;
      }
    }

    animation.addEventListener('DOMLoaded', loadedEvent);
    animation.wrapper.addEventListener('mouseenter', mouseEnterEvent);
    animation.wrapper.addEventListener('click', clickEvent);
    animation.wrapper.addEventListener('mouseleave', mouseLeaveEvent);
  };

  /** Initialize every single icons **/
  var logoAnimation = initializeAnimation('__dp_logo', 'dp-logo.json');
  registerEvent(logoAnimation,
    function() { logoAnimation.playSegments([0, 49], true); logoAnimation.loop=true; },
    function() { logoAnimation.playSegments([50, 75], true); logoAnimation.loop=false; },
    function() { logoAnimation.playSegments([75, 124], true); },
    function() { logoAnimation.playSegments([0, 49], true); logoAnimation.loop=true; }
  );
} ) ( jQuery );