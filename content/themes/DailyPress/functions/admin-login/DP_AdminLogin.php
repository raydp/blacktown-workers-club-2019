<?php
/**
 * Module Name: Daily Press Admin Login
 * Author: Bryan Sebastian
 *
 * This module will override the default admin login page
 * of WordPress and this is dedicated for Daily Press projects only
 *
 * @package DP_AdminLogin
 * @version 1.0.0
 */
class DP_AdminLogin {
  const MODULE_PATH = '/functions/admin-login';

  function __construct() {
    add_action( 'login_enqueue_scripts', [ $this, 'moduleEnqueueAssets' ], 10 );
  }

  function moduleEnqueueAssets() {
    wp_enqueue_style( 'dp-admin-login', get_template_directory_uri() . self::MODULE_PATH . '/assets/css/dp-admin-login.css', false );

    wp_enqueue_script( 'lottie', get_template_directory_uri() . self::MODULE_PATH . '/assets/js/lottie.min.js' );
    wp_enqueue_script( 'dp-admin-login', get_template_directory_uri() . self::MODULE_PATH . '/assets/js/dp-admin-login.js', array( 'jquery' ), false, true );

    // Localize the script with new data
    $translation_array = array(
      'dir' => get_template_directory_uri() . self::MODULE_PATH,
    );
    wp_localize_script( 'dp-admin-login', 'localized_obj', $translation_array );
  }
}

new DP_AdminLogin;
