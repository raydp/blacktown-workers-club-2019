<?php
$labels = array(
    'name'               => _x( 'Sports & Facilities', 'post type general name' ),
    'singular_name'      => _x( 'Single Club', 'post type singular name' ),
    'add_new'            => __( 'Add New Club' ),
    'add_new_item'       => __( 'Add New Club' ),
    'edit_item'          => __( 'Edit Club' ),
    'new_item'           => __( 'New Club' ),
    'all_items'          => __( 'All Clubs' ),
    'view_item'          => __( 'View Club' ),
    'search_items'       => __( 'Search Club' ),
    'not_found'          => __( 'No Clubs Found' ),
    'not_found_in_trash' => __( 'No Clubs Found in Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => __( 'Sports & Facilities' )
);

$args = array(
    'labels'             => $labels,
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true, 
    'show_in_menu'       => true, 
    'query_var'          => true,
    'rewrite'            => true,
    'capability_type'    => 'post',
    'has_archive'        => true, 
    'hierarchical'       => false,
    'supports'           => array( 'title', 'editor', 'thumbnail' ),
); 

register_post_type( 'sports-facilities', $args );