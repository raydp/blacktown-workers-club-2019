<?php
function sports_facilities_list( $atts ) {
    extract(
        shortcode_atts(
            array(),
            $atts
        )
    );

    ob_start();

    $sports_facilities = new WP_Query( [
        'post_type'      => 'sports-facilities',
        'posts_per_page' => -1,
        'orderby'        => 'date',
        'order'          => 'ASC'
    ] );
    ?>
    <section class="sports-facilities">
        <div class="sports-facilities__container">
            <div class="row">
                <?php if ( $sports_facilities->have_posts() ): ?>
                    <?php while( $sports_facilities->have_posts() ): ?>
                        <?php
                            $sports_facilities->the_post();

                            $img_url = get_the_post_thumbnail_url( get_the_ID(), 'full' );
                            $contact_person = get_field( 'contact_person', get_the_ID() );
                            $contact_number = get_field( 'contact_number', get_the_ID() );
                            $report = get_field( 'report', get_the_ID() );
                            $link_url = empty(get_field('website')['url']) ? get_permalink(get_the_ID()) : get_field('website')['url'];
                            $link_url_target =  empty(get_field('website')['url']) ? '_self' : '_blank';
                        ?>
                        <div class="single-club large-3 medium-6 small-12">
                            <figure>
                                <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo $link_url_target; ?>";>
                                    <img src="<?php echo esc_url( $img_url ); ?>" alt="<?php echo get_the_title() ?>">
                                </a>
                                <figcaption>
                                    <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo $link_url_target; ?>";><h5><?php the_title() ?></h5></a>
                                    <p><?php echo $contact_person ?></p>
                                    <p><?php echo $contact_number ?></p>
                                    <?php if ( $report ): ?>
                                        <div class="section_report">
                                        <a href="<?php echo esc_url( $report['url'] ) ?>" class="dl_report" target="_blank">Download Report</a>
                                        </div>
                                    <?php endif ?>
                                </figcaption>
                            </figure>
                        </div>
                    <?php endwhile ?>
                <?php else: ?>
                    <div class="columns">
                        <p class="no-result">No Sporting Clubs Available</p>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </section>
    
    <?php
    wp_reset_postdata();

    return ob_get_clean();
}

add_action( 'init', function() {
   add_shortcode( 'sports-facilities-list', 'sports_facilities_list' );
} );
