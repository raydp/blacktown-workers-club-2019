<?php
// Register menus
register_nav_menus(
        array(
                'main-nav' => __( 'The Main Menu', 'jointswp' ),   // Main nav in header
                'footer-links' => __( 'Footer Links', 'jointswp' ) // Secondary nav in footer
        )
      );

// Daily Press Plain Menu
function dp_plain_menu() {
     wp_nav_menu(array(
        'container' => false,                           // Remove nav container
        'menu_class' => 'dp-plain-menu',       // Adding custom nav class
        'items_wrap' => '<ul class="%2$s">%3$s</ul>',
        'theme_location' => 'main-nav',                 // Where it's located in the theme
        'depth' => 2,                                   // Limit the depth of the nav
        'fallback_cb' => false,                         // Fallback function (see below)
        'walker' => new DP_Topbar_Menu_Walker()
    ));
}

// Daily Press Custom Menu
function dp_custom_menu($atts, $content = null) {
    extract(shortcode_atts(array( 'name' => null, 'class' => null ), $atts));
    return wp_nav_menu( array( 'menu' => $name, 'menu_class' => 'myclass', 'echo' => false ) );
}

add_shortcode('dp_custom_menu', 'dp_custom_menu');

// Daily Press Custom Sidebar Menu
function dp_nav_sidebar() {
         wp_nav_menu(array(
        'container' => false,                           // Remove nav container
        'menu_class' => 'horizontal-menu',       // Adding custom nav class
        'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
        'theme_location' => 'main-nav',                         // Where it's located in the theme
        'depth' => 5,                                   // Limit the depth of the nav
        'fallback_cb' => false,                         // Fallback function (see below)
        'walker' => new Dp_Topbar_Menu_Walker()
    ));
}

// Daily Press Custom Menu Walker
class Dp_Topbar_Menu_Walker extends Walker_Nav_Menu {
    function start_lvl(&$output, $depth = 0, $args = Array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"second-level-menu\">\n";
        // $output .= "\n\t$indent<li class=\"second-level-parent menu-item\"></li>\n";
    }
}


// Add Foundation active class to menu
function required_active_nav_class( $classes, $item ) {
    if ( $item->current == 1 || $item->current_item_ancestor == true ) {
        $classes[] = 'active';
    }
    return $classes;
}
add_filter( 'nav_menu_css_class', 'required_active_nav_class', 10, 2 );
