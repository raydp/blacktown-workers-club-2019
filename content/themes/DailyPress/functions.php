<?php
/**
 * For more info: https://developer.wordpress.org/themes/basics/theme-functions/
 *
 */

// Theme support options
require_once(get_template_directory().'/functions/theme-support.php');

// WP Head and other cleanup functions
require_once(get_template_directory().'/functions/cleanup.php');

// Register scripts and stylesheets
require_once(get_template_directory().'/functions/enqueue-scripts.php');

// Register custom menus and menu walkers
require_once(get_template_directory().'/functions/menu.php');

// Daily Press custom functions
require_once(get_template_directory().'/functions/dp-custom-functions.php');

// Daily Press custom admin login
require_once(get_template_directory().'/functions/admin-login/DP_AdminLogin.php');

// Sporting Club
require_once(get_template_directory().'/functions/sports-facilities/post-type.php');
require_once(get_template_directory().'/functions/sports-facilities/shortcode.php');

// Daily Press membership renewals
require_once(get_template_directory().'/functions/membership-renewals/membership-post-type.php');
require_once(get_template_directory().'/functions/membership-renewals/membership-form.php');

/*
* Creating a function to create custom post type
*/
 
function custom_post_type() {
 
    // Set UI labels for Custom Post Type
        $labels = array(
            'name'                => _x( 'Weekly Activities', 'Post Type General Name', 'DailyPress' ),
            'singular_name'       => _x( 'Single Activity', 'Post Type Singular Name', 'DailyPress' ),
            'menu_name'           => __( 'Weekly Activities', 'DailyPress' ),
            'parent_item_colon'   => __( 'Parent Single Activity', 'DailyPress' ),
            'all_items'           => __( 'All Weekly Activities', 'DailyPress' ),
            'view_item'           => __( 'View Single Activity', 'DailyPress' ),
            'add_new_item'        => __( 'Add New Activity', 'DailyPress' ),
            'add_new'             => __( 'Add New', 'DailyPress' ),
            'edit_item'           => __( 'Edit Activity', 'DailyPress' ),
            'update_item'         => __( 'Update Activity', 'DailyPress' ),
            'search_items'        => __( 'Search Activity', 'DailyPress' ),
            'not_found'           => __( 'Not Found', 'DailyPress' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'DailyPress' ),
        );
         
    // Set other options for Custom Post Type
         
        $args = array(
            'label'               => __( 'weekly activities', 'DailyPress' ),
            'description'         => __( 'Activity Details', 'DailyPress' ),
            'labels'              => $labels,
            // Features this CPT supports in Post Editor
            'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', 'custom-fields', ),
            // You can associate this CPT with a taxonomy or custom taxonomy. 
            // 'taxonomies'          => array( 'genres' ),
            /* A hierarchical CPT is like Pages and can have
            * Parent and child items. A non-hierarchical CPT
            * is like Posts.
            */ 
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
            'taxonomies'          => array( 'category' ),
        );
         
        // Registering your Custom Post Type
        register_post_type( 'weekly_activities', $args );
     
    }
     
  /* Hook into the 'init' action so that the function
  * Containing our post type registration is not 
  * unnecessarily executed. 
  */
    
  add_action( 'init', 'custom_post_type', 0 );
  
  add_filter('pre_get_posts', 'query_post_type');
  function query_post_type($query) {
  if( is_category() ) {
    $post_type = get_query_var('post_type');
    if($post_type)
        $post_type = $post_type;
    else
        $post_type = array('nav_menu_item', 'post', 'weekly_ctivities'); // don't forget nav_menu_item to allow menus to work!
    $query->set('post_type',$post_type);
    return $query;
    }
  }
  
  add_action( 'pre_get_posts', 'add_my_post_types_to_query' );
  
  function add_my_post_types_to_query( $query ) {
    if ( is_home() && $query->is_main_query() )
        $query->set( 'post_type', array( 'post', 'weekly_activities' ) );
    return $query;
  }

/**************
 * Display Posts Shortcodes
 **************/

function shapeSpace_recent_posts_shortcode($atts, $content = null) {
	
	global $post;
	
	extract(shortcode_atts(array(
		'cat'     => '',
		'num'     => '-1',
		'order'   => 'DESC',
		'orderby' => 'post_date',
	), $atts));
	
	$args = array(
		'cat'            => $cat,
		'posts_per_page' => $num,
		'order'          => $order,
		'orderby'        => $orderby,
	);
	
	$output = '';
	$posts = get_posts($args);
	
	foreach($posts as $post) {
		setup_postdata($post);
		$output .= '<li class="single-post small-12 medium-6 large-4"><a href="'. get_the_permalink() .'">';
		$output .= '<div class="background-image">';
		$output .= '<img class="lazy" data-src="' . get_the_post_thumbnail_url(null, 'full') .  '">';
		$output .= '</div>';
		$output .= '<div class="content">';
		$output .= '<h3>' . get_the_title() . '</h3>';
		$output .= '</div>';
		$output .= '</a></li>';
	}
	
	wp_reset_postdata();
	
	return '<ul class="list-posts grid-x">'. $output .'</ul>';
	
}
add_shortcode('list_posts', 'shapeSpace_recent_posts_shortcode');