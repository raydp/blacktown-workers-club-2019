<?php
/**
 * List View Single Event
 * This file contains one event in the list view
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/single-event.php
 *
 * @version 4.6.19
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

// Setup an array of venue details for use later in the template
$venue_details = tribe_get_venue_details();

// The address string via tribe_get_venue_details will often be populated even when there's
// no address, so let's get the address string on its own for a couple of checks below.
$venue_address = tribe_get_address();

// Venue
$has_venue_address = ( ! empty( $venue_details['address'] ) ) ? ' location' : '';

// Organizer
$organizer = tribe_get_organizer();

?>
<div class="single-event__container">
	<a class="tribe-event-url" href="<?php echo esc_url( tribe_get_event_link() ); ?>" title="<?php the_title_attribute() ?>" rel="bookmark">
		<!-- Background Image -->
		<div class="dp-list-events-background-image">
			<img src="<?php echo get_the_post_thumbnail_url( null, 'full' ); ?>">
		</div>
		<!-- Event Content -->
		<div class="dp-list-events-content">
			
			<!-- Event Title -->
			<?php do_action( 'tribe_events_before_the_event_title' ) ?>
			<h6 class="tribe-events-list__event-title">
				
				<?php the_title() ?>
				
			</h6>
			<?php do_action( 'tribe_events_after_the_event_title' ) ?>

			<!-- Event Meta -->
			<?php do_action( 'tribe_events_before_the_meta' ) ?>
			<div class="tribe-events__event-meta">

				<!-- Schedule & Recurrence Details -->
				<div class="tribe-event__schedule-details">
					<?php echo tribe_get_start_date(get_the_ID(), false, 'l g:i A'); ?>
				</div>
				<div class="tribe-event__short-conclusion">
					<?php echo get_field('event_short_conclusion')?>
				</div>
			</div><!-- .tribe-events-event-meta -->


			<!-- <div class="dp-tribe-events-button-group">
				<?php if(tribe_get_event_meta( null, '_EventURL', true )): ?>
					<a class="dp-single-button" href="<?php echo tribe_get_event_meta( null, '_EventURL', true ) ?>" aria-label="Event Ticket Link" target="_blank"><button class="button">TICKETS</button></a>
				<?php endif; ?>
				<a class="dp-single-button" href="<?php echo esc_url( tribe_get_event_link() ); ?>" aria-label="Event Detail Link"><button class="button">MORE INFO</button></a>
			</div> -->
		</div>
	</a>
</div>