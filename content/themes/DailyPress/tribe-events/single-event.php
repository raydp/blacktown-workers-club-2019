<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural   = tribe_get_event_label_plural();

$event_id = get_the_ID();
$home_dir = get_home_url();

$event_banner = '/content/uploads/2019/11/Whats-On-Events.jpg';
if(!empty(get_field('event_banner'))){
	$event_banner = get_field('event_banner')['url'];
}

?>
<section class="slider">
    <div class="slider__container">
		<?php 
			$background_image = '/content/uploads/2019/11/Whats-On-Events.jpg';
			$title = "What's On";
		?>
		<!-- <div class="single-slide" style="background-image: url(<?php echo $event_banner; ?>)"> -->
		<div class="single-slide lazy" data-src="<?php echo $event_banner; ?>">
			<div class="single-slide__container grid-y">
				<h1 class="title"><?php echo $title; ?></h1>
			</div>
		</div>
    </div>
</section>
<div id="tribe-events-content" class="tribe-events-single">
	<div class="dp-single-event-main-content row">
		<div class="dp-left-section small-12 large-6">
			<!-- <?php echo tribe_event_featured_image( $event_id, 'full', false ); ?> -->
			<img class="lazy" data-src="<?php echo get_the_post_thumbnail_url($event_id, 'full'); ?>">
		</div>
		<div class="dp-right-section small-12 large-6">
			<!-- Notices -->
			<?php tribe_the_notices() ?>

			<?php the_title( '<h1 class="tribe-events-single-event-title">', '</h1>' ); ?>

			<div class="tribe-events-schedule tribe-clearfix">
				<h2>
					<span><?php echo tribe_get_start_date(get_the_ID(), false, 'l, jS F Y'); ?> @ <?php echo tribe_get_start_time(get_the_ID()); ?></span>
				</h2>
			</div>

			<!-- Cost -->
			<?php if ( tribe_get_cost() ) : ?>
				<div class="dp-event-cost">
					<span class="tribe-events-cost">Price: <?php echo tribe_get_cost( null, true ) ?></span>
				</div>
			<?php endif; ?>
			
			
			<!-- Event content -->
			<?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
			<div class="tribe-events-single-event-description tribe-events-content">
				<?php the_content(); ?>
			</div>

			<!-- Event facebook link -->
			<?php if(get_field('facebook_event_link')): ?>
				<div class="dp-events-facebook-link">
					<span>For further event details please visit: </span><a class="dp-facebook-button" href="<?php echo get_field('facebook_event_link'); ?>" aria-label="Event Facebook Link" target="_blank"><img class="event-facebook-button" src="<?php echo $home_dir; ?>/content/themes/DailyPress/assets/images/facebook.png"></a>
				</div>
			<?php endif; ?>
			<!-- Event ticket link -->
			<?php if(tribe_get_event_meta( null, '_EventURL', true )): ?>
				<div class="dp-events-ticket-link">
					<a class="dp-ticket-button" href="<?php echo tribe_get_event_meta( null, '_EventURL', true ) ?>" aria-label="Event Ticket Link" target="_blank"><button>Book Now!</button></a>
				</div>
			<?php endif; ?>
		</div>
	</div>


</div><!-- #tribe-events-content -->