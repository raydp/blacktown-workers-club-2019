# Daily Press - Base Template #
### Wordpress Template ###
All file structure, wordpress installation is here to get you started on a new website project.
Basically, it is just a plain Wordpress template, without anything on it. The main advantages of this template is the **folder structure, hot reload, and scss**.

### Get it running ###

###### Install Environment #######
First thing first, you need to have **npm** installed in your computer refer [here](https://nodejs.org/en/).

If you have installed **npm**, follow this:  

* `cd wordpress/content/themes/DailyPress/`  
* `npm install`  

###### Setup Wordpress ######
* create new database
* edit wp-config.php with the database username password
* change site name on the database
    * open database
    * edit dp_options table
         * siteurl -> Change to new site url (leave */wp* at the end) (*e.g: `http://newwebsite.dev/wp`*)
         * home    -> Change to new home url (*e.g: `http://newwebsite.dev`*)

###### Development ######
* ```cd wordpress/content/themes/DailyPress/```
* edit `gulpfile.js`
    * edit `LOCAL_URL ` with your local development setup. (e.g. `http://newwebsite.dev/`)
* ```gulp browsersync```
* start editing, anything you change will automatically updated on the website

###### File Structure ######

**IMPORTANT**: Please read this through before proceeding

    wordpress
    ├── wp                                    #base wordpress files
    ├── index.php               
    ├── wp-config.php                         #wordpress configuration files
    ├── content             
    │   ├── plugin
    │   ├── themes
    │   │   ├── DailyPress
    │   │   │   ├── assets                    #all the assets here
    │   │   │   │   ├── styles                
    │   │   │   │   │   ├── styles.css        #generated css
    │   │   │   │   │   ├── scss              
    │   │   │   │   │   |   ├── partials      #styles separated to be a little chunk in a different file
    │   │   │   │   │   |   ├── partials.scss  #IMPORTANT: every new scss file needs to be imported here  
    │   │   │   │   │   |   └── ...
    │   │   │   │   │   └── ...
    │   │   │   │   ├── scripts
    │   │   │   │   │   ├── js                #all javascript should be all in this folder
    │   │   │   │   │   └── ...               #other files are just generated files
    │   │   │   │   └── ...               
    │   │   │   ├── template                  #all wordpress template should be inside this folder
    │   │   │   │   ├── snippet               #smaller piece of template
    │   │   │   │   └── ...               
    │   │   │   └── ...               
    │   │   └── ...               
    │   └── ...               
    └── ...

### Admin Credentials ###
Username: albert_dev  
Password: 3tent55



#### #Note

If you find a lot of missing 404 link, the case may be that the wordpress installation is inside a sub-folder of your `public_html` folder (e.g.`http://localhost/newwebsite/`).

Please set up the files at the root of your localhost (e.g. `http://newwebsite.dev`). So that the dev environment is similar to the live website.